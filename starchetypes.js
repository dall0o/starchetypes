function reset() {
    var el = document.querySelector('.article-content > div:nth-child(80) > table:nth-child(1) > tbody:nth-child(3)'),
        elClone = el.cloneNode(true);

    el.parentNode.replaceChild(elClone, el);
}

function getFeatures(tr) {
    return [...tr.querySelectorAll('td:not(:first-child)')].map(x => x.textContent.includes('X') || x.textContent.includes('C'))
}

function isStackable(a, b){
    for (var i = 0; i < a.length; i++){
        if (a[i] & b[i])
            return false;
    }
    return true;
}

reset();
var rows = document.querySelectorAll('.article-content > div:nth-child(80) > table:nth-child(1) > tbody:nth-child(3) > tr');

Array.from(rows).forEach(link => {
    link.style.backgroundColor = 'white';
    link.addEventListener('click', function (event) {

        const features = getFeatures(this);
        
        Array.from(rows).forEach(row => {

            if (isStackable(features, getFeatures(row))) {
                row.style.backgroundColor = 'hsla(120,100%,50%,0.5)';
            }
            else {
                row.style.backgroundColor = 'hsla(0,100%,50%,0.5)';
            }
        });

        
        this.style.backgroundColor = 'hsla(240,100%,50%,0.5)';
    });
});